variable "vpc_cidr_block" {
	description = "Bloco CIDR destinado para equipe team-cielo"
	default = "10.22.128.0/20"
}

variable "aws_region" {
	description = "Região para dev N. Virginia"
	default = "us-east-1"
}

variable "m4u_team_id" {
	default = "9"
}

variable "vpn_psk" {
	description = "Senha PSK para tunel VPN"
	default = "k5E1eFWfvf76V3O4"
}

variable "vpn_ip" {
	description = "IP da VPN M4u"
	default = "186.228.60.130"
}

variable "vpn_cidr" {
	default = "10.10.0.0/15"
}

variable "key_name" {
	default = "arthur.stockler"
}

variable "amazon_linux_ami" {
	description = "Região para dev N. Virginia"
	default = "ami-08111162"
}

variable "availability_zones" {
  description = "The availability zones"
  default = ["us-east-1a", "us-east-1d"]
}

variable "ec2_cli_access_key" {
  description = "Chave usada pelas instancias EC2 terem acesso ao aws cli"
  default = "AKIAJWKXPR4DUL5XT55A"
}

variable "ec2_cli_secret_key" {
  description = "Chave usada pelas instancias EC2 terem acesso ao aws cli"
  default = "voLmnBSCLB/Tgow8DkMZbGz2DmXC0nisZ05zxWnk"
}
