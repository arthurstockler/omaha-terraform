#!/bin/sh
TF_DBNAME="order-rds"

echo aws rds describe-db-instances --db-instance-identifier=$TF_DBNAME --query 'DBInstances[0].[DBInstanceStatus]'

STATUS_DB=$(aws rds describe-db-instances --db-instance-identifier=$TF_DBNAME --query 'DBInstances[0].[DBInstanceStatus]')
echo $STATUS_DB

if [[ $STATUS_DB == *"available"* ]]; then
echo "DBNAME available" >> /home/ec2-user/trace.log
break;
else
echo "esperando..." >> /home/ec2-user/trace.log
fi;