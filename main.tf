/* Resources */
provider "aws" {
  region  = "${var.aws_region}"
  profile = "saml"
}

/* Criando uma VPC */
module "vpc" {
    source         = "./modules/vpc"
    vpc_cidr_block = "${var.vpc_cidr_block}"
}

/* Funcoes Lambda base */
module "lambda" {
    source         = "./modules/lambda"
}

/* Subnet DMZ e Web */
module "subnets" {
    source             = "./modules/subnets"
    vpcid              = "${module.vpc.vpcid}"
    vpc_cidr_block     = "${var.vpc_cidr_block}"
    availability_zones = "${var.availability_zones}"
    vpnid              = "${module.vpn.vpnid}"
    vpn_cidr           = "${var.vpn_cidr}"
    sg_nat             = "${module.security-group.sg_nat_id}"
    key_name           = "${var.key_name}"
}

/* Criando SG */
module "security-group" {
    source           = "./modules/security-group"
    vpcid            = "${module.vpc.vpcid}"
    vpc_cidr_block   = "${var.vpc_cidr_block}"
    web_cidr_block   = "${module.subnets.subnet_web_a_id}"
}

/* Instancia de VPN */
module "vpn" {
    source                 = "./modules/vpn"
    vpn_ip                 = "${var.vpn_ip}"
    vpn_psk                = "${var.vpn_psk}"
    vpn_cidr               = "${var.vpn_cidr}"
    vpc_cidr_block         = "${var.vpc_cidr_block}"
    m4u_team_id            = "${var.m4u_team_id}"
    subnet_dmza            = "${module.subnets.subnet_dmz_a_id}"
    key_name               = "${var.key_name}"
    vpc_security_group_ids = "${module.security-group.sg_nat_id},${module.security-group.sg_ipsec_id},${module.security-group.sg_admin_id}"
}

/* Helios Master */
module "helios-master" {
    source          = "./modules/helios"
    key_name        = "${var.key_name}"
    subnet_id       = "${module.subnets.subnet_master_control_id}"
    subnet_cidr     = "${module.subnets.master_cidr_block}"
    sg_nat          = "${module.security-group.sg_nat_id}"
    sg_admin        = "${module.security-group.sg_admin_id}"
    sg_helios       = "${module.security-group.sg_helios_id}"
}

/* Instancia de RDS Order */
module "rds-order" {
    source             = "./modules/rds"
    subnet_rdsa_id     = "${module.subnets.subnet_rdsa_id}"
    subnet_rdsd_id     = "${module.subnets.subnet_rdsd_id}"
    sg_pg_admin        = "${module.security-group.sg_pg_admin_id}"
    key_name           = "${var.key_name}"
    sg_nat             = "${module.security-group.sg_nat_id}"
    sg_admin           = "${module.security-group.sg_admin_id}"
    vpcid              = "${module.vpc.vpcid}"
    weba_cidr_block    = "${module.subnets.weba_cidr_block}"
    rdsa_cidr_block    = "${module.subnets.rdsa_cidr_block}"
    rdsd_cidr_block    = "${module.subnets.rdsd_cidr_block}"
    aws_region         = "${var.aws_region}"
    ec2_cli_access_key = "${var.ec2_cli_access_key}"
    ec2_cli_secret_key = "${var.ec2_cli_secret_key}"
}

/* JMeter Master, controla os escravos que irao fazer o teste */
module "jmeter-master" {
    source          = "./modules/jmeters/master"
    key_name        = "${var.key_name}"
    subnet_id       = "${module.subnets.subnet_master_control_id}"
    sg_nat          = "${module.security-group.sg_nat_id}"
    sg_admin        = "${module.security-group.sg_admin_id}"
    sg_jmtmaster    = "${module.security-group.sg_jmtmaster_id}"
}

/* JMeter Slaves para teste de carga */
module "jmeter-slaves" {
    source          = "./modules/jmeters/slaves"
    key_name        = "${var.key_name}"
    subnet_id       = "${module.subnets.jmt_slaves_id}"
    sg_nat          = "${module.security-group.sg_nat_id}"
    sg_admin        = "${module.security-group.sg_admin_id}"
    sg_jmtslave     = "${module.security-group.sg_jmtslave_id}"
}

/* Micro servico de Order Manager */
module "order-manager" {
    source              = "./modules/microservices/order"
    key_name            = "${var.key_name}"
    subnet_id           = "${module.subnets.subnet_web_a_id}"
    sg_nat              = "${module.security-group.sg_nat_id}"
    sg_admin            = "${module.security-group.sg_admin_id}"
    sg_web              = "${module.security-group.sg_web_id}"
    aws_region          = "${var.aws_region}"
    ec2_cli_access_key  = "${var.ec2_cli_access_key}"
    ec2_cli_secret_key  = "${var.ec2_cli_secret_key}"
    db_name             = "${module.rds-order.db_name}"
    db_pass             = "${module.rds-order.db_password}"
    db_user             = "${module.rds-order.db_username}"
    db_endpoint         = "${module.rds-order.db_endpoint}"
    helios_master_ip    = "${module.helios-master.helios_pvt_ip}"
}
