all: plan apply

config:
	terraform remote config -backend=s3 -backend-config="bucket=m4u-infra-cielo" -backend-config="key=team-cielo-dev-vpc/terraform.tfstate" -backend-config="region=us-east-1";
	echo "OK" > .config;

plan:
	terraform get;
	terraform plan;

apply: plan
	terraform apply;

destroy:
	terraform destroy -force;
	test -s .config && rm .config;

retry: destroy all

clean:
	rm -fR .terraform/;
	rm .config;
