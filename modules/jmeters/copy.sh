#!/bin/bash

JM_MASTER="$1"
NUM_USER="$2"
NUM_SEC="$3"
NUM_LOOP="$4"
RANGE_HOST="$5"
ORDER_MANAGER_IP="$6"

HOSTNAME=$(hostname -I | awk '{print $1}')

restartSlaves(){

  for i in $(echo $RANGE_HOST | tr "," "\n")
  do
    echo "REINICIANDO JSLAVES: $i"
    ssh -i ../../arthur.stockler.pem ubuntu@$i "sudo docker restart slave"
  done

  execute

}

execute () {
  #copiar o base
  cp resource/order_base.jmx order_test.jmx

  #MUDANDO ARQUIVO ORDER TEST
  sed -i -e "s/m4u_om_user/$NUM_USER/g" order_test.jmx
  sed -i -e "s/m4u_om_sec/$NUM_SEC/g" order_test.jmx
  sed -i -e "s/m5u_loop/$NUM_LOOP/g" order_test.jmx
  sed -i -e "s/m4u_om_ip/$ORDER_MANAGER_IP/g" order_test.jmx

  #COPIAR PARA HOST REMOTO
  scp -i ../../arthur.stockler.pem order_test.jmx ubuntu@$JM_MASTER:/home/ubuntu
  rm order_test.jmx

  #COPIANDO PARA DENTRO DO DOCKER
  ssh -i ../../arthur.stockler.pem ubuntu@$JM_MASTER "sudo docker exec -i jmaster sh -c 'cat > order_test.jmx' < order_test.jmx"

  #apaga apos a copia
  ssh -i ../../arthur.stockler.pem ubuntu@$JM_MASTER "sudo rm order_test.jmx"

  #MUDANDO ARQUIVO RUN TEST JMETER
  cp resource/jmt_base.sh jmt_order.sh
  sed -i -e "s/M4U_JMX_FILE/order_test.jmx/g" jmt_order.sh
  sed -i -e "s/M4U_HOSTNAME/$JM_MASTER/g" jmt_order.sh
  sed -i -e "s/M4U_RANGE_HOSTS/$RANGE_HOST/g" jmt_order.sh

  #COPIAR PARA HOST REMOTO
  scp -i ../../arthur.stockler.pem jmt_order.sh ubuntu@$JM_MASTER:/home/ubuntu
  rm jmt_order.sh

  #COPIANDO PARA DENTRO DO DOCKER
  ssh -i ../../arthur.stockler.pem ubuntu@$JM_MASTER "sudo docker exec -i jmaster sh -c 'cat > jmt_order.sh' < jmt_order.sh"
  ssh -i ../../arthur.stockler.pem ubuntu@$JM_MASTER "sudo docker exec -i jmaster sh -c 'chmod +777 jmt_order.sh'"

  #apaga apos a copia
  ssh -i ../../arthur.stockler.pem ubuntu@$JM_MASTER "sudo rm jmt_order.sh"

  #logando
  ssh -i ../../arthur.stockler.pem ubuntu@$JM_MASTER
}

if [ -z "$RANGE_HOST" ]; then
    RANGE_HOST=$HOSTNAME
fi

if [ -z "$NUM_USER" ]; then
    NUM_USER=0
fi

if [ -z "$NUM_SEC" ]; then
    NUM_SEC=0
fi

if [ -z "$NUM_LOOP" ]; then
    NUM_LOOP=1
fi

if [ "$NUM_SEC" -ge 0 ] && [ "$NUM_USER" -ge 0 ]; then
  restartSlaves
fi
