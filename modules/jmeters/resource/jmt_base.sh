#!/bin/bash

echo "INICIANDO TESTE"

jmeter -n -t M4U_JMX_FILE -Djava.rmi.server.hostname=M4U_HOSTNAME -Dclient.rmi.localport=60000 -RM4U_RANGE_HOSTS -l result_report.csv -e -o result_report/

echo "GERANDO REPORT"

NAME_ZIP="result_report_$(date '+%y-%m-%d').tar.gz"

echo $NAME_ZIP

#zipando resultado
tar -czvf $NAME_ZIP result_report/

#remove lixo
rm result_report.csv
rm -rf result_report/

echo "FINALIZADO"
