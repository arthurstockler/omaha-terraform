resource "aws_instance" "jmeter-master" {
    ami                      = "ami-40d28157"
    instance_type            = "t2.nano"
    subnet_id                = "${var.subnet_id}"
    key_name                 = "${var.key_name}"
    source_dest_check        = false
    vpc_security_group_ids   = ["${var.sg_nat}", "${var.sg_admin}", "${var.sg_jmtmaster}"]
    user_data                = "${data.template_file.jmeter-master-install.rendered}"
    tags {
        "Name"        = "jmeter-master"
        "client"      = "cielo-lio"
        "role"        = "lab"
        "project"     = "backendlio"
    }
}

data "template_file" "jmeter-master-install" {
    template = "${file("${path.module}/jmeter_master.sh")}"
}
