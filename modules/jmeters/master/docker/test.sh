#!/bin/bash

JMX_FILE=$1
HOSTNAME=$2
RANGE_HOSTS=$3

jmeter -n -t $JMX_FILE -Djava.rmi.server.hostname=$HOSTNAME -Dclient.rmi.localport=60000 -R$RANGE_HOSTS
