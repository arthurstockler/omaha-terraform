#http://www.testautomationguru.com/jmeter-distributed-load-testing-using-docker-in-aws/

1 -> Criando docker
Crie um arquivo Dockerfile

2 -> Compilando
docker build -t=usuario_xxx/projeto .
#docker build -t arthurstockler/jmeter_master .
#docker build -t arthurstockler/jmeter_slave .

3 -> Empurrando docker para nuvem
docker push arthurstockler/jmeter_master
docker push arthurstockler/jmeter_slave

scp -i ../../macos.pem order_test.jmx ubuntu@10.22.128.61:/home/ubuntu


##########################################################################################
sudo docker stop $(sudo docker ps -a -q)
sudo docker rm $(sudo docker ps -a -q)

#copiando arquivo para dentro do docker
sudo docker exec -i jmaster sh -c 'cat > /jmeter/apache-jmeter-3.0/bin/order_test.jmx' < order_test.jmx

#entrando no docker
sudo docker exec -it jmaster /bin/bash

#executando teste
cd /jmeter/apache-jmeter-3.0/bin
./jmeter -n -t order_test.jmx -Djava.rmi.server.hostname=10.22.128.61 -Dclient.rmi.localport=60000 -R10.22.128.71

./jmeter -n -t test.jmx -Djava.rmi.server.hostname=10.22.128.58 -Dclient.rmi.localport=60000 -R10.22.128.71

./jmeter -Jjmeter.save.saveservice.output_format=xml -Jjmeter.save.saveservice.response_data=true -Jjmeter.save.saveservice.samplerData=true -Jjmeter.save.saveservice.requestHeaders=true -Jjmeter.save.saveservice.url=true -Jjmeter.save.saveservice.responseHeaders=true -n -t test.jmx -l test_result.jtl

sudo docker run -d -i -t -p 60000:60000 arthurstockler/jmeter_master /bin/bash

#lista ps
netstat -lptu

#report
jmeter -n -t <test JMX file> -l <test log file> -e -o <Path to output folder>

#compactar
tar -czvf result_order.tar.gz result_order/

#docker copy
docker cp jmaster:/result_order.tar.gz result_order.tar.gz

# copy report
scp -i ../../arthur.stockler.pem ubuntu@10.22.128.54:/home/ubuntu/result_order.tar.gz result_order.tar.gz

scp user@host:file ~/Desktop/file




jmeter.save.saveservice.bytes = true
jmeter.save.saveservice.label = true
jmeter.save.saveservice.latency = true
jmeter.save.saveservice.response_code = true
jmeter.save.saveservice.response_message = true
jmeter.save.saveservice.successful = true
jmeter.save.saveservice.thread_counts = true
jmeter.save.saveservice.thread_name = true
jmeter.save.saveservice.time = true
jmeter.save.saveservice.timestamp_format = s
jmeter.save.saveservice.timestamp_format = yyyy/MM/dd HH:mm:ss
jmeter.reportgenerator.report_title=Order Manager
jmeter.reportgenerator.overall_granularity=60000
