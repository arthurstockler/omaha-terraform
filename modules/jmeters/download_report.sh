#!/bin/bash

JM_MASTER="$1"
NAME_ZIP="result_report_$(date '+%y-%m-%d').tar.gz"
NAME_DIC=$(date '+%y-%m-%d-%HH-%MM')
NAME_OUT="result_report_$NAME_DIC.tar.gz"

echo $NAME_ZIP
echo $NAME_OUT

# copy report
scp -i ../../arthur.stockler.pem ubuntu@$JM_MASTER:/home/ubuntu/$NAME_ZIP reports/$NAME_OUT

#apaga apos a copia
ssh -i ../../arthur.stockler.pem ubuntu@$JM_MASTER "sudo rm -f $NAME_ZIP"

#extrando report
mkdir reports/$NAME_DIC
cd reports/$NAME_DIC
tar -zxvf ../$NAME_OUT
