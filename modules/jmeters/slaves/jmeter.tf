resource "aws_instance" "jmeter-slave" {
    count                    = 2
    ami                      = "ami-40d28157"
    instance_type            = "t2.small"
    subnet_id                = "${var.subnet_id}"
    key_name                 = "${var.key_name}"
    source_dest_check        = false
    vpc_security_group_ids   = ["${var.sg_nat}", "${var.sg_admin}", "${var.sg_jmtslave}"]
    user_data                = "${data.template_file.jmeter-slave-install.rendered}"
    tags {
        "Name"        = "jmeter-slave-${count.index}"
        "client"      = "cielo-lio"
        "role"        = "lab"
        "project"     = "backendlio"
    }
}

data "template_file" "jmeter-slave-install" {
    template = "${file("${path.module}/jmeter_slave.sh")}"
}
