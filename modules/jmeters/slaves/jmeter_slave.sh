#!/bin/sh -x

 # Wait for internet connectivity
RETRY_COUNT=900;
until test $$RETRY_COUNT -eq 0; do
  if ping -c1 8.8.8.8 > /dev/null; then
    break;
  elif ping -c1 8.8.4.4 > /dev/null; then
    break;
  else
    sleep 2;
    RETRY_COUNT=$(($RETRY_COUNT-1));
  fi;
done;

### Install Docker
sudo apt-get update
sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
echo "deb https://apt.dockerproject.org/repo ubuntu-xenial main" | sudo tee /etc/apt/sources.list.d/docker.list
sudo apt-get update
apt-cache policy docker-engine
sudo apt-get install -y docker-engine
sudo usermod -aG docker ubuntu

####COLOCANDO DOCKER JMASTER NA INICIALIZACAO
cat << EOF > /etc/systemd/system/docker-jmeter_server.service

[Unit]
Description=Jmeter Container
Requires=docker.service
After=docker.service

[Service]
Restart=always
ExecStart=/usr/bin/docker start -a slave
ExecStop=/usr/bin/docker stop -t 2 slave

[Install]
WantedBy = multi-user.target

EOF

systemctl enable docker-jmeter_server.service
systemctl daemon-reload
systemctl start docker-jmeter_server.service

### Install Jmeter slave
HOSTNAME=$(hostname -I | awk '{print $1}')
sudo docker run -d -i -t -p 1099:1099 -p 50000:50000 -e LOCALIP=$HOSTNAME --name slave arthurstockler/jmeter_slave /bin/bash
