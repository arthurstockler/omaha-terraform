variable "key_name" {
	description = "nome da chave de acesso a instancia"
}

variable "sg_nat" {
	description = "security group para rede NAT"
}

variable "sg_admin" {
	description = "para permitir acesso SSH as maquinas"
}

variable "subnet_id" {
	description = "Id da subnet privada"
}

variable "sg_jmtslave" {
	description = "permite acesso a porta do jmeter slave"
}
