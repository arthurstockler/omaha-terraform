#!/bin/sh -x
sudo su

echo ">> ATUALIZANDO" >> /home/ec2-user/trace.log
yum -y update

echo ">> DOWNLOAD DOCKER" >> /home/ec2-user/trace.log
curl -sSL https://get.docker.com/ | sh
groupadd docker
usermod -aG docker ec2-user

echo ">> CONFIG DOCKER" >> /home/ec2-user/trace.log
echo "{ 'auths': { 'https://index.docker.io/v1/': { 'auth': 'xXxXxXxXxXx=' } } }" >> /home/ec2-user/.docker/config.json

echo ">> START DOCKER" >> /home/ec2-user/trace.log
service docker start

yum install -y java-1.8.0
yum remove -y java-1.7.0-openjdk


curl -sSL https://s3-sa-east-1.amazonaws.com/m4u-tc-infra-tools/helios-agent.tar.bz2 | \
	    sudo tar -jxC /opt
chown -R ec2-user: /opt/helios-*

echo "$(hostname -I | cut -d' ' -f1) $(hostname)" >> /etc/hosts

master=54.70.152.226:2181
labels="app=api-call-back"
host=$(hostname)

su - ec2-user <<-EOF
 java -cp /opt/helios-agent/helios-services.jar com.spotify.helios.agent.AgentMain \
 --zk $master --labels $labels --id $host --name $host
EOF
