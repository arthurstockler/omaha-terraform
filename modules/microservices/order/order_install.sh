#!/bin/sh -x

 # Wait for internet connectivity
RETRY_COUNT=900;
until test $$RETRY_COUNT -eq 0; do
  if ping -c1 8.8.8.8 > /dev/null; then
    break;
  elif ping -c1 8.8.4.4 > /dev/null; then
    break;
  else
    sleep 2;
    RETRY_COUNT=$(($RETRY_COUNT-1));
  fi;
done;

### Pegando variaveis
TF_HELIOS_MASTER="${helios_master_ip}"
TF_HELIOS_ORDER_LABEL="${helios_order_app}"
TF_NEW_RELIC_KEY="${newrelic_key}"
TF_RAILS_ENV="${rails_env}"
TF_ACCESS_KEY="${aws_access_key}"
TF_SECRET_KEY="${aws_secret_key}"
TF_REGION="${aws_region}"
TF_DBNAME="${db_name}"
TF_DBPASS="${db_pass}"
TF_DBUSER="${db_user}"
TF_DBENDPOINT="${db_endpoint}"
L_DATABASE_URL="postgres://$TF_DBUSER:$TF_DBPASS@$TF_DBENDPOINT/postgres"
HOSTNAME=$(hostname -I | awk '{print $1}')

######### LOG
echo "(1) $L_DATABASE_URL" >> /home/ubuntu/trace.log
echo "(2) postgres://myuser:changepass@$TF_DBNAME/order-rds" >> /home/ubuntu/trace.log
echo $TF_ACCESS_KEY >> /home/ubuntu/trace.log
echo $TF_SECRET_KEY >> /home/ubuntu/trace.log
echo $TF_REGION >> /home/ubuntu/trace.log
echo $TF_DBNAME >> /home/ubuntu/trace.log
echo $TF_DBPASS >> /home/ubuntu/trace.log
echo $TF_DBUSER >> /home/ubuntu/trace.log
echo $TF_DBENDPOINT >> /home/ubuntu/trace.log
echo $L_DATABASE_URL >> /home/ubuntu/trace.log
echo $HOSTNAME >> /home/ubuntu/trace.log
echo $TF_HELIOS_MASTER >> /home/ubuntu/trace.log
echo $TF_HELIOS_ORDER_LABEL >> /home/ubuntu/trace.log

### Install Java 8
sudo apt-get update
sudo apt-get install -y default-jre

### Install Docker
sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
echo "deb https://apt.dockerproject.org/repo ubuntu-xenial main" | sudo tee /etc/apt/sources.list.d/docker.list
sudo apt-get update
apt-cache policy docker-engine
sudo apt-get install -y docker-engine
sudo usermod -aG docker ubuntu


echo "Criando docker config" >> /home/ubuntu/trace.log
### Criando Docker config
cd /root
mkdir .docker
cat << EOF > /root/.docker/config.json
  {
    "auths": {
      "https://index.docker.io/v1/": {
        "auth": "YXJ0aHVyc3RvY2tsZXI6YWozMDEwODE=",
        "email": "arthur.stockler@m4u.com.br"
      }
    },
    "HttpHeaders": {
      "User-Agent": "Docker-Client/1.11.1 (linux)"
    }
  }
EOF


echo "Criando AWS Credentials" >> /home/ubuntu/trace.log
### AWS Credentials
cd /root
mkdir .aws
cat << EOF > /root/.aws/credentials
[default]
region = $TF_REGION
aws_access_key_id = $TF_ACCESS_KEY
aws_secret_access_key = $TF_SECRET_KEY
output = json
EOF

echo "Exportando variaveis" >> /home/ubuntu/trace.log
### Exportando variaveis
echo "export AWS_ACCESS_KEY=$TF_ACCESS_KEY" >> /root/.bashrc
echo "export AWS_SECRET_KEY=$TF_SECRET_KEY" >> /root/.bashrc
echo "export AWS_DEFAULT_REGION=$TF_REGION" >> /root/.bashrc
export AWS_ACCESS_KEY=$TF_ACCESS_KEY
export AWS_SECRET_KEY=$TF_SECRET_KEY
export AWS_DEFAULT_REGION=$TF_REGION

### Install Helios Agent
curl -sSL https://s3-sa-east-1.amazonaws.com/m4u-tc-infra-tools/helios-agent.tar.bz2 | sudo tar -jxC /opt
sudo nohup java -cp /opt/helios-agent/helios-services.jar com.spotify.helios.agent.AgentMain --zk $TF_HELIOS_MASTER --labels "$TF_HELIOS_ORDER_LABEL" --id $HOSTNAME --name $HOSTNAME &

docker run -d -it -p "8085:8080" -h $HOSTNAME -e AWS_"ACCESS_KEY_ID=$TF_ACCESS_KEY" -e "AWS_SECRET_ACCESS_KEY=$TF_SECRET_KEY" -e "AWS_REGION=$TF_REGION" -e "QUEUE_NAME=order-manager-api-callback" -e "QUEUE_NAME_FAILURES=order-manager-api-callback-dlq" -e "RAILS_ENV=$TF_RAILS_ENV" -e "NEW_RELIC_KEY=$TF_NEW_RELIC_KEY" -e "DATABASE_URL=$L_DATABASE_URL" m4ucorp/cielolio:cielo-lio-order.24-1e1d215
