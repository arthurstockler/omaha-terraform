variable "key_name" {}

variable "sg_nat" {
		description = "security group para rede interna"
}

variable "sg_admin" {
		description = "para permitir acesso SSH as maquinas"
}

variable "sg_web" {
		description = "security group para acesso as portas do docker order"
}

variable "subnet_id" {
		description = "Id da subnet privada de web"
}

variable "aws_region" {
		description = "Região para dev N. Virginia"
}

variable "ec2_cli_access_key" {
		description = "Chave usada pelas instancias EC2 terem acesso ao aws cli"
}

variable "ec2_cli_secret_key" {
		description = "Chave usada pelas instancias EC2 terem acesso ao aws cli"
}

variable "db_name" {
		description = "Nome do banco de dados"
}

variable "db_pass" {
		description = "Senha do banco de dados"
}

variable "db_user" {
		description = "usuario do banco de dados"
}

variable "db_endpoint" {
		description = "Endpoint do banco de dados"
}

variable "helios_master_ip" {
		description = "Ip do Helios master"
}

variable "helios_order_app" {
		description = "Nome do app no Helios"
		default = "app=order-manager"
}

variable "newrelic_key" {
		description = "Chave do new relic"
		default = "5027e872caff3c9872e09e8c5604ec98a0ff098a"
}
