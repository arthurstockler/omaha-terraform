resource "aws_instance" "order" {
    count             = 1
    ami               = "ami-40d28157"
    instance_type     = "t2.micro"
    subnet_id         = "${var.subnet_id}"
    key_name          = "${var.key_name}"
    source_dest_check = false
    security_groups   = ["${var.sg_nat}", "${var.sg_admin}", "${var.sg_web}"]
    user_data         = "${data.template_file.order-docker-install.rendered}"
    tags {
        "Name"        = "order-manager-${count.index}"
        "client"      = "cielo-lio"
        "role"        = "lab"
        "project"     = "backendlio"
    }
}


data "template_file" "order-docker-install" {
    template = "${file("${path.module}/order_install.sh")}"
    vars {
        aws_access_key   = "${var.ec2_cli_access_key}"
        aws_secret_key   = "${var.ec2_cli_secret_key}"
        aws_region       = "${var.aws_region}"
        db_name          = "${var.db_name}"
        db_pass          = "${var.db_pass}"
        db_user          = "${var.db_user}"
        db_endpoint      = "${var.db_endpoint}"
        rails_env        = "PRODUCTION"
        helios_master_ip = "${var.helios_master_ip}"
        helios_order_app = "${var.helios_order_app}"
        newrelic_key     = "${var.newrelic_key}"
    }
}
