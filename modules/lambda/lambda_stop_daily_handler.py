import boto3

def lambda_handler(event, context):

    ec2 = boto3.resource('ec2', region_name='us-west-2')
    
    instances = ec2.instances.filter(Filters=[{'Name': 'instance-state-name', 'Values': ['running']}])

    for instance in instances:
        print(instance.id, instance.instance_type)
    
    RunningInstances = [instance.id for instance in instances]
    
    if len(RunningInstances) > 0:
        shuttingDown = ec2.instances.filter(InstanceIds=RunningInstances).stop()
        print shuttingDown