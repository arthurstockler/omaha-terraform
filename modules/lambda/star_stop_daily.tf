resource "aws_iam_role" "iam_for_lambda_daily" {
    name = "iam_for_lambda_daily"
    assume_role_policy = <<EOF
{
   "Version":"2012-10-17",
   "Statement":[
      {
         "Action":"sts:AssumeRole",
         "Principal":{
            "Service":"lambda.amazonaws.com"
         },
         "Effect":"Allow"
      }
   ]
}
	EOF
}

resource "aws_iam_role_policy" "lambda_daily_policy" {
    name = "lambda_daily_policy"
    role = "${aws_iam_role.iam_for_lambda_daily.id}"
    policy = <<EOF
{
   "Version":"2012-10-17",
   "Statement":[
      {
         "Effect":"Allow",
         "Action":[
            "logs:CreateLogGroup",
            "logs:CreateLogStream",
            "logs:PutLogEvents"
         ],
         "Resource":"arn:aws:logs:*:*:*"
      },
      {
         "Effect":"Allow",
         "Action":[
            "ec2:Start*",
            "ec2:Stop*",
            "ec2:DescribeInstances"
         ],
         "Resource":"*"
      }
   ]
}
	EOF
}

resource "aws_lambda_function" "start_ec2_daily" {
    function_name    = "start_ec2_daily"
    description      = "Funcao para iniciar as maquinas EC2"
    runtime          = "python2.7"
    role             = "${aws_iam_role.iam_for_lambda_daily.arn}"
    handler          = "lambda_start_daily_handler.lambda_handler"
    filename         = "${path.module}/handlers.zip"
    source_code_hash = "${base64sha256(file("${path.module}/handlers.zip"))}"
}

resource "aws_lambda_function" "stop_ec2_daily" {
    function_name    = "stop_ec2_daily"
    description      = "Funcao para parar as maquinas EC2"
    runtime          = "python2.7"
    role             = "${aws_iam_role.iam_for_lambda_daily.arn}"
    handler          = "lambda_stop_daily_handler.lambda_handler"
    filename         = "${path.module}/handlers.zip"
    source_code_hash = "${base64sha256(file("${path.module}/handlers.zip"))}"
}

resource "aws_cloudwatch_event_rule" "start_ec2_rule" {
  name                = "start_ec2_daily"
  description         = "Agenda horario para iniciar as instancias EC2"
  schedule_expression = "cron(0 8 * * ? *)"
  is_enabled          = "true"
}

resource "aws_cloudwatch_event_rule" "stop_ec2_rule" {
  name                = "stop_ec2_daily"
  description         = "Agenda horario para parar as instancias EC2"
  schedule_expression = "cron(0 22 * * ? *)"
  is_enabled          = "true"
}

resource "aws_cloudwatch_event_target" "star_ec2_daily_trigger" {
    rule      = "${aws_cloudwatch_event_rule.start_ec2_rule.name}"
    target_id = "start_ec2_daily"
    arn       = "${aws_lambda_function.start_ec2_daily.arn}"
}

resource "aws_cloudwatch_event_target" "stop_ec2_daily_trigger" {
    rule      = "${aws_cloudwatch_event_rule.stop_ec2_rule.name}"
    target_id = "stop_ec2_daily"
    arn       = "${aws_lambda_function.stop_ec2_daily.arn}"
}

resource "aws_lambda_permission" "allow_cloudwatch_to_call_start_ec2_rule" {
    statement_id  = "AllowExecutionFromCloudWatch"
    action        = "lambda:InvokeFunction"
    function_name = "${aws_lambda_function.start_ec2_daily.function_name}"
    principal     = "events.amazonaws.com"
    source_arn    = "${aws_cloudwatch_event_rule.start_ec2_rule.arn}"
}

resource "aws_lambda_permission" "allow_cloudwatch_to_stop_call_ec2_rule" {
    statement_id  = "AllowExecutionFromCloudWatch"
    action        = "lambda:InvokeFunction"
    function_name = "${aws_lambda_function.stop_ec2_daily.function_name}"
    principal     = "events.amazonaws.com"
    source_arn    = "${aws_cloudwatch_event_rule.stop_ec2_rule.arn}"
}

