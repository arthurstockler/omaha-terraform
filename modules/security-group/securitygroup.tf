resource "aws_security_group" "sg_nat" {
    name        = "nat-rules"
    description = "Permite trafego da rede interna"
    vpc_id      = "${var.vpcid}"
    ingress {
            from_port = 0
            to_port = 0
            protocol = "-1" /*-1 ALL Traffic*/
            cidr_blocks = ["${var.vpc_cidr_block}"]
    }

    egress {
            from_port = 0
            to_port = 0
            protocol = "-1" /*-1 ALL Traffic*/
            cidr_blocks = ["0.0.0.0/0"]
    }

    tags {
            "Name" = "nat-rules"
         }
}

/* TODO: VPN */
resource "aws_security_group" "ipsec" {
    name = "vpn-ipsec"
    description = "IPSec related ports"
    vpc_id = "${var.vpcid}"

    ingress {
            from_port = 500
            to_port = 500
            protocol = "17" /*17 UDP*/
            cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
            from_port = 500
            to_port = 500
            protocol = "17" /*17 UDP*/
            cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
            from_port = 4500
            to_port = 4500
            protocol = "17" /*17 UDP*/
            cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
            from_port = 4500
            to_port = 4500
            protocol = "17" /*17 UDP*/
            cidr_blocks = ["0.0.0.0/0"]
    }

    tags {
            Name = "vpn-ipsec"
            Tier = "Infrastructure"
            Zone = "-"
            Team = ""
    }
}

/*Libera acessos as administracao via SSH (22) Acess remote*/
resource "aws_security_group" "admin" {
    name = "admin-ssh"
    description = "Security group liberando acesso de ssh, acesso remoto"
    vpc_id = "${var.vpcid}"

    ingress {
            from_port = 22
            to_port = 22
            protocol = "6"
            cidr_blocks = ["10.10.0.0/15" , "10.17.16.0/23"]
    }

    ingress {
            from_port = 3389
            to_port = 3389
            protocol = "6"
            cidr_blocks = ["10.10.0.0/15"]
    }

    egress {
            from_port = 0
            to_port = 0
            protocol = "-1"
            cidr_blocks = ["0.0.0.0/0"]
    }

    tags {
            Name = "admin-ssh"
    }
}

/*Libera acesso ao PG Admin*/
resource "aws_security_group" "sql_admin" {
    name = "admin-database"
    description = "Security Group Liberanco acesso de banco de dados"
    vpc_id = "${var.vpcid}"

    ingress {
            from_port = 5432
            to_port = 5432
            protocol = "6"
            cidr_blocks = ["10.10.0.0/15"]
    }

    ingress {
            from_port = 5432
            to_port = 5432
            protocol = "17"
            cidr_blocks = ["10.10.0.0/15"]
    }

    egress {
            from_port = 0
            to_port = 0
            protocol = "-1"
            cidr_blocks = ["0.0.0.0/0"]
    }

    tags {
            Name = "admin-database"
    }
}

/* Security group web */
resource "aws_security_group" "web" {
    name = "web"
    description = "Permite acesso ao trafego de web"
    vpc_id = "${var.vpcid}"

    ingress {
        from_port = 8085
        to_port   = 8085
        protocol  = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port = 443
        to_port   = 443
        protocol  = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port = 8089
        to_port   = 8089
        protocol  = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port = -1
        to_port = -1
        protocol = "icmp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
            from_port = 0
            to_port = 0
            protocol = "-1"
            cidr_blocks = ["0.0.0.0/0"]
    }
    tags {
        Name = "web"
    }
}

/* Security group web */
resource "aws_security_group" "helios" {
    name = "helios-master"
    description = "Permite acesso ao trafego helios"
    vpc_id = "${var.vpcid}"

    ingress {
        from_port = 5801
        to_port   = 5801
        protocol  = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port = 2181
        to_port   = 2181
        protocol  = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
            from_port = 0
            to_port = 0
            protocol = "-1"
            cidr_blocks = ["0.0.0.0/0"]
    }

    tags {
        Name = "helios-master"
    }
}

/* Security group web */
resource "aws_security_group" "jmeter-master" {
    name = "jmeter-master"
    description = "Permite acesso ao trafego jmeter"
    vpc_id = "${var.vpcid}"

    ingress {
        from_port = 60000
        to_port   = 60000
        protocol  = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
            from_port = 0
            to_port = 0
            protocol = "-1"
            cidr_blocks = ["0.0.0.0/0"]
    }

    tags {
        Name = "jmeter-master"
    }
}

resource "aws_security_group" "jmeter-slave" {
    name = "jmeter-slave"
    description = "Permite acesso ao trafego jmeter"
    vpc_id = "${var.vpcid}"

    ingress {
        from_port = 1099
        to_port   = 1099
        protocol  = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port = 50000
        to_port   = 50000
        protocol  = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
            from_port = 0
            to_port = 0
            protocol = "-1"
            cidr_blocks = ["0.0.0.0/0"]
    }

    tags {
        Name = "jmeter-slave"
    }
}
