variable "vpcid" {
    description = "Identificador da vpc"
}

variable "vpc_cidr_block" {
    description = "Bloco CIDR do time"
}

variable "web_cidr_block" {
    description = "Bloco CIDR para subnet WEB"
}
