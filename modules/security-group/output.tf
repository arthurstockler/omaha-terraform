output "sg_nat_id" {
	value = "${aws_security_group.sg_nat.id}"
}

output "sg_ipsec_id" {
	value = "${aws_security_group.ipsec.id}"
}

output "sg_admin_id" {
	value = "${aws_security_group.admin.id}"
}

output "sg_pg_admin_id" {
	value = "${aws_security_group.sql_admin.id}"
}

output "sg_web_id" {
	value = "${aws_security_group.web.id}"
}

output "sg_helios_id" {
	value = "${aws_security_group.helios.id}"
}

output "sg_jmtmaster_id" {
	value = "${aws_security_group.jmeter-master.id}"
}

output "sg_jmtslave_id" {
	value = "${aws_security_group.jmeter-slave.id}"
}
