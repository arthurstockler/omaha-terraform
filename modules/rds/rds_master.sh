#!/bin/sh -x

TF_ACCESS_KEY="${aws_access_key}"
TF_SECRET_KEY="${aws_secret_key}"
TF_REGION="${aws_region}"
TF_DBNAME="${db_name}"
TF_DBPASS="${db_pass}"
TF_DBUSER="${db_user}"
TF_DBENDPOINT="${db_endpoint}"
TF_DDLFILE="${ddl_file}"

echo $TF_ACCESS_KEY >> /home/ec2-user/trace.log
echo $TF_SECRET_KEY >> /home/ec2-user/trace.log
echo $TF_REGION >> /home/ec2-user/trace.log
echo $TF_DBNAME >> /home/ec2-user/trace.log
echo $TF_DBPASS >> /home/ec2-user/trace.log
echo $TF_DBUSER >> /home/ec2-user/trace.log
echo $TF_DBENDPOINT >> /home/ec2-user/trace.log

# Wait for internet connectivity
RETRY_COUNT=900;
until test $$RETRY_COUNT -eq 0; do
  if ping -c1 8.8.8.8 > /dev/null; then
    break;
  elif ping -c1 8.8.4.4 > /dev/null; then
    break;
  else
    sleep 1;
    RETRY_COUNT=$(($RETRY_COUNT-1));
  fi;
done;

#atualizando repo
echo ">> ATUALIZANDO" >> /home/ec2-user/trace.log
sudo yum -y update

#install postgresql
echo ">> INSTALANDO POSTGRESQL" >> /home/ec2-user/trace.log
sudo yum install -y postgresql94

#exportando a senha
echo ">> EXPORTANDO VARIAVEIS" >> /home/ec2-user/trace.log
export PGPASSWORD=$TF_DBPASS
export AWS_ACCESS_KEY=$TF_ACCESS_KEY
export AWS_SECRET_KEY=$TF_SECRET_KEY
export AWS_DEFAULT_REGION=$TF_REGION

#ALTERANDO BASH
echo ">> ALTERANDO BASH" >> /home/ec2-user/trace.log
echo "export PGPASSWORD=$TF_DBPASS" >> /home/ec2-user/.bash_profile
echo "export AWS_ACCESS_KEY=$TF_ACCESS_KEY" >> /home/ec2-user/.bash_profile
echo "export AWS_SECRET_KEY=$TF_SECRET_KEY" >> /home/ec2-user/.bash_profile
echo "export AWS_DEFAULT_REGION=$TF_REGION" >> /home/ec2-user/.bash_profile

sudo source /home/ec2-user/.bash_profile

#CRIANDO ARQUIVO AWS CREDENTIALS
echo ">> CRIANDO ARQUIVO AWS CREDENTIALS" >> /home/ec2-user/trace.log
cd /home/ec2-user/
mkdir .aws
cd /home/ec2-user/.aws
echo "[default]" >> /home/ec2-user/.aws/credentials
echo "region = $TF_REGION" >> /home/ec2-user/.aws/credentials
echo "aws_access_key_id = $TF_ACCESS_KEY" >> /home/ec2-user/.aws/credentials
echo "aws_secret_access_key = $TF_SECRET_KEY" >> /home/ec2-user/.aws/credentials
echo "output = json" >> /home/ec2-user/.aws/credentials

cd /home/ec2-user
