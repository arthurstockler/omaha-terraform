resource "aws_db_subnet_group" "default" {
    name        = "sub-group-db"
    description = "Grupo de subnets RDS DB"
    subnet_ids  = ["${var.subnet_rdsa_id}", "${var.subnet_rdsd_id}"]
}

resource "aws_db_event_subscription" "order-subscription" {
    name        = "rds-order-event-sub"
    sns_topic   = "${aws_sns_topic.rds-events-order.arn}"
    source_type = "db-instance"
    source_ids  = ["${aws_db_instance.rds_order_instance.identifier}"]
    enabled     = true
}

resource "aws_sns_topic" "rds-events-order" {
    name = "rds-events-order"
}

resource "aws_sns_topic_subscription" "sub-order-ddl-create" {
    topic_arn = "${aws_sns_topic.rds-events-order.arn}"
    protocol = "lambda"
    endpoint = "${aws_lambda_function.rds_order_ddl.arn}"
}

resource "aws_lambda_function" "rds_order_ddl" {
    function_name    = "rds_order_ddl"
    description      = "Funcao criar a DDL do BD"
    runtime          = "python2.7"
    role             = "${var.iam_for_lambda_daily_arn}"
    handler          = "lambda_rds_order_ddl_handler.lambda_handler"
    filename         = "${path.module}/rds-order-ddl.zip"
    source_code_hash = "${base64sha256(file("${path.module}/rds-order-ddl.zip"))}"
}