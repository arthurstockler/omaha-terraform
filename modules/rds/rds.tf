resource "aws_db_instance" "rds_order_instance" {
    depends_on             = ["aws_db_subnet_group.default"]
    identifier             = "order-rds"
    allocated_storage      = "10"
    engine                 = "postgres"
    storage_type           = "gp2"
    engine_version         = "9.4.1"
    instance_class         = "db.t2.micro"
    username               = "${var.db_username}"
    password               = "${var.db_password}"
    vpc_security_group_ids = ["${var.sg_pg_admin}", "${aws_security_group.rds_control_sql_admin.id}", "${aws_security_group.web-rds-access.id}"]
    db_subnet_group_name   = "${aws_db_subnet_group.default.id}"

    tags {
      tag_name = "order-rds"
      role     = "DB"
      pci      = "false"
    }
}

resource "aws_db_subnet_group" "default" {
    name        = "sub-group-db"
    description = "Grupo de subnets RDS DB"
    subnet_ids  = ["${var.subnet_rdsa_id}", "${var.subnet_rdsd_id}"]
}


resource "aws_instance" "rds-manager" {
    ami                      = "${data.aws_ami.amazon_ami_rds_master.image_id}"
    instance_type            = "t2.nano"
    subnet_id                = "${var.subnet_rdsa_id}"
    key_name                 = "${var.key_name}"
    source_dest_check        = false
    vpc_security_group_ids   = ["${var.sg_nat}", "${var.sg_admin}"]
    tags {
        "Name"        = "rds-manager"
        "client"      = "cielo-lio"
    }

    connection {
        user        = "ec2-user"
        private_key = "${file("arthur.stockler.pem")}"
        agent       = false
    }

    provisioner "remote-exec" {
        inline = [
          "sleep 30",
          "sudo yum -y update",
          "sudo yum install -y postgresql94",
          "echo 'export PGPASSWORD=${var.db_password}' >> /home/ec2-user/.bash_profile",
          "echo 'export AWS_ACCESS_KEY=${var.ec2_cli_access_key}' >> /home/ec2-user/.bash_profile",
          "echo 'export AWS_SECRET_KEY=${var.ec2_cli_secret_key}' >> /home/ec2-user/.bash_profile",
          "echo 'export AWS_DEFAULT_REGION=${var.aws_region}' >> /home/ec2-user/.bash_profile",
          "source /home/ec2-user/.bash_profile",
          "echo '- CRIANDO ARQUIVO AWS CREDENTIALS - ' >> /home/ec2-user/trace.log",
          "cd /home/ec2-user/",
          "mkdir .aws",
          "cd /home/ec2-user/.aws",
          "echo '[default]' >> /home/ec2-user/.aws/credentials",
          "echo 'region = ${var.aws_region}' >> /home/ec2-user/.aws/credentials",
          "echo 'aws_access_key_id = ${var.ec2_cli_access_key}' >> /home/ec2-user/.aws/credentials",
          "echo 'aws_secret_access_key = ${var.ec2_cli_secret_key}' >> /home/ec2-user/.aws/credentials",
          "echo 'output = json' >> /home/ec2-user/.aws/credentials",
          "sleep 5",
          "aws s3 cp s3://cielo-omaha-staging/check_db.sh /home/ec2-user/check_db.sh",
          "sleep 10",
          "chmod +x /home/ec2-user/check_db.sh",
          "/home/ec2-user/check_db.sh ${aws_db_instance.rds_order_instance.identifier} ${var.db_ddl} ${aws_db_instance.rds_order_instance.address} ${var.db_username}"
        ]
    }
}

data "aws_ami" "amazon_ami_rds_master" {
  most_recent = true
  filter {
    name = "owner-alias"
    values = [
      "amazon"]
  }
  filter {
    name = "name"
    values = [
      "amzn-ami-hvm-*-gp2"]
  }
}

resource "aws_security_group" "rds_control_sql_admin" {
    name = "admin_rds_manager"
    description = "Security Group Liberanco acesso de banco de dados"
    vpc_id = "${var.vpcid}"

    ingress {
            from_port = 5432
            to_port = 5432
            protocol = "6"
            cidr_blocks = ["${var.rdsa_cidr_block}"]
    }

    ingress {
            from_port = 5432
            to_port = 5432
            protocol = "17"
            cidr_blocks = ["${var.rdsa_cidr_block}"]
    }


    ingress {
            from_port = 5432
            to_port = 5432
            protocol = "6"
            cidr_blocks = ["${var.rdsd_cidr_block}"]
    }

    ingress {
            from_port = 5432
            to_port = 5432
            protocol = "17"
            cidr_blocks = ["${var.rdsd_cidr_block}"]
    }

    egress {
            from_port = 0
            to_port = 0
            protocol = "-1"
            cidr_blocks = ["0.0.0.0/0"]
    }

    tags {
            Name = "admin-rds-manager"
    }
}

resource "aws_security_group" "web-rds-access" {
    name = "web-rds-access"
    description = "Security Group Liberanco acesso da subnet web para banco de dados"
    vpc_id = "${var.vpcid}"

    ingress {
            from_port = 5432
            to_port = 5432
            protocol = "6"
            cidr_blocks = ["${var.weba_cidr_block}"]
    }

    ingress {
            from_port = 5432
            to_port = 5432
            protocol = "17"
            cidr_blocks = ["${var.weba_cidr_block}"]
    }

    egress {
            from_port = 0
            to_port = 0
            protocol = "-1"
            cidr_blocks = ["0.0.0.0/0"]
    }

    tags {
          Name = "web-rds-access"
    }
}
