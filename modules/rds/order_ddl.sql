--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.2
-- Dumped by pg_dump version 9.5.1

-- Started on 2016-10-18 15:10:12

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 13276)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 3137 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 189 (class 1259 OID 26487)
-- Name: cards; Type: TABLE; Schema: public; Owner: sysadm
--

CREATE TABLE cards (
    id integer NOT NULL,
    brand character varying,
    mask character varying,
    payment_transaction_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE cards OWNER TO sysadm;

--
-- TOC entry 188 (class 1259 OID 26485)
-- Name: cards_id_seq; Type: SEQUENCE; Schema: public; Owner: sysadm
--

CREATE SEQUENCE cards_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cards_id_seq OWNER TO sysadm;

--
-- TOC entry 3138 (class 0 OID 0)
-- Dependencies: 188
-- Name: cards_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sysadm
--

ALTER SEQUENCE cards_id_seq OWNED BY cards.id;


--
-- TOC entry 185 (class 1259 OID 26447)
-- Name: items; Type: TABLE; Schema: public; Owner: sysadm
--

CREATE TABLE items (
    id integer NOT NULL,
    sku character varying,
    name character varying,
    description character varying,
    unit_price integer,
    quantity integer DEFAULT 1,
    unit_of_measure character varying,
    details character varying,
    reference character varying,
    uuid character varying,
    order_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE items OWNER TO sysadm;

--
-- TOC entry 184 (class 1259 OID 26445)
-- Name: items_id_seq; Type: SEQUENCE; Schema: public; Owner: sysadm
--

CREATE SEQUENCE items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE items_id_seq OWNER TO sysadm;

--
-- TOC entry 3139 (class 0 OID 0)
-- Dependencies: 184
-- Name: items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sysadm
--

ALTER SEQUENCE items_id_seq OWNED BY items.id;


--
-- TOC entry 195 (class 1259 OID 26542)
-- Name: merchants; Type: TABLE; Schema: public; Owner: sysadm
--

CREATE TABLE merchants (
    id integer NOT NULL,
    name character varying,
    number character varying,
    email character varying,
    uuid character varying(40),
    notification_url character varying,
    deleted_at timestamp without time zone,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE merchants OWNER TO sysadm;

--
-- TOC entry 194 (class 1259 OID 26540)
-- Name: merchants_id_seq; Type: SEQUENCE; Schema: public; Owner: sysadm
--

CREATE SEQUENCE merchants_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE merchants_id_seq OWNER TO sysadm;

--
-- TOC entry 3140 (class 0 OID 0)
-- Dependencies: 194
-- Name: merchants_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sysadm
--

ALTER SEQUENCE merchants_id_seq OWNED BY merchants.id;


--
-- TOC entry 183 (class 1259 OID 26428)
-- Name: orders; Type: TABLE; Schema: public; Owner: sysadm
--

CREATE TABLE orders (
    id integer NOT NULL,
    external_id character varying,
    number character varying,
    reference character varying,
    status integer,
    notes character varying,
    price integer,
    remaining integer DEFAULT 0,
    paid_amount integer DEFAULT 0,
    merchant_id character varying(40),
    uuid character varying(40),
    deleted_at timestamp without time zone,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE orders OWNER TO sysadm;

--
-- TOC entry 182 (class 1259 OID 26426)
-- Name: orders_id_seq; Type: SEQUENCE; Schema: public; Owner: sysadm
--

CREATE SEQUENCE orders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE orders_id_seq OWNER TO sysadm;

--
-- TOC entry 3141 (class 0 OID 0)
-- Dependencies: 182
-- Name: orders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sysadm
--

ALTER SEQUENCE orders_id_seq OWNED BY orders.id;


--
-- TOC entry 191 (class 1259 OID 26504)
-- Name: payment_products; Type: TABLE; Schema: public; Owner: sysadm
--

CREATE TABLE payment_products (
    id integer NOT NULL,
    number integer,
    name character varying,
    payment_transaction_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE payment_products OWNER TO sysadm;

--
-- TOC entry 190 (class 1259 OID 26502)
-- Name: payment_products_id_seq; Type: SEQUENCE; Schema: public; Owner: sysadm
--

CREATE SEQUENCE payment_products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE payment_products_id_seq OWNER TO sysadm;

--
-- TOC entry 3142 (class 0 OID 0)
-- Dependencies: 190
-- Name: payment_products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sysadm
--

ALTER SEQUENCE payment_products_id_seq OWNED BY payment_products.id;


--
-- TOC entry 193 (class 1259 OID 26523)
-- Name: payment_services; Type: TABLE; Schema: public; Owner: sysadm
--

CREATE TABLE payment_services (
    id integer NOT NULL,
    number integer,
    name character varying,
    payment_product_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE payment_services OWNER TO sysadm;

--
-- TOC entry 192 (class 1259 OID 26521)
-- Name: payment_services_id_seq; Type: SEQUENCE; Schema: public; Owner: sysadm
--

CREATE SEQUENCE payment_services_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE payment_services_id_seq OWNER TO sysadm;

--
-- TOC entry 3143 (class 0 OID 0)
-- Dependencies: 192
-- Name: payment_services_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sysadm
--

ALTER SEQUENCE payment_services_id_seq OWNED BY payment_services.id;


--
-- TOC entry 187 (class 1259 OID 26466)
-- Name: payment_transactions; Type: TABLE; Schema: public; Owner: sysadm
--

CREATE TABLE payment_transactions (
    id integer NOT NULL,
    uuid character varying,
    external_id character varying,
    transaction_type character varying,
    status character varying,
    description character varying,
    terminal_number character varying,
    number character varying,
    authorization_code character varying,
    amount integer,
    order_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE payment_transactions OWNER TO sysadm;

--
-- TOC entry 186 (class 1259 OID 26464)
-- Name: payment_transactions_id_seq; Type: SEQUENCE; Schema: public; Owner: sysadm
--

CREATE SEQUENCE payment_transactions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE payment_transactions_id_seq OWNER TO sysadm;

--
-- TOC entry 3144 (class 0 OID 0)
-- Dependencies: 186
-- Name: payment_transactions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sysadm
--

ALTER SEQUENCE payment_transactions_id_seq OWNED BY payment_transactions.id;


--
-- TOC entry 181 (class 1259 OID 26419)
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: sysadm
--

CREATE TABLE schema_migrations (
    version character varying NOT NULL
);


ALTER TABLE schema_migrations OWNER TO sysadm;

--
-- TOC entry 197 (class 1259 OID 26556)
-- Name: terminals; Type: TABLE; Schema: public; Owner: sysadm
--

CREATE TABLE terminals (
    id integer NOT NULL,
    number character varying,
    uuid character varying(40),
    merchant_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE terminals OWNER TO sysadm;

--
-- TOC entry 196 (class 1259 OID 26554)
-- Name: terminals_id_seq; Type: SEQUENCE; Schema: public; Owner: sysadm
--

CREATE SEQUENCE terminals_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE terminals_id_seq OWNER TO sysadm;

--
-- TOC entry 3145 (class 0 OID 0)
-- Dependencies: 196
-- Name: terminals_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sysadm
--

ALTER SEQUENCE terminals_id_seq OWNED BY terminals.id;


--
-- TOC entry 2961 (class 2604 OID 26490)
-- Name: id; Type: DEFAULT; Schema: public; Owner: sysadm
--

ALTER TABLE ONLY cards ALTER COLUMN id SET DEFAULT nextval('cards_id_seq'::regclass);


--
-- TOC entry 2958 (class 2604 OID 26450)
-- Name: id; Type: DEFAULT; Schema: public; Owner: sysadm
--

ALTER TABLE ONLY items ALTER COLUMN id SET DEFAULT nextval('items_id_seq'::regclass);


--
-- TOC entry 2964 (class 2604 OID 26545)
-- Name: id; Type: DEFAULT; Schema: public; Owner: sysadm
--

ALTER TABLE ONLY merchants ALTER COLUMN id SET DEFAULT nextval('merchants_id_seq'::regclass);


--
-- TOC entry 2955 (class 2604 OID 26431)
-- Name: id; Type: DEFAULT; Schema: public; Owner: sysadm
--

ALTER TABLE ONLY orders ALTER COLUMN id SET DEFAULT nextval('orders_id_seq'::regclass);


--
-- TOC entry 2962 (class 2604 OID 26507)
-- Name: id; Type: DEFAULT; Schema: public; Owner: sysadm
--

ALTER TABLE ONLY payment_products ALTER COLUMN id SET DEFAULT nextval('payment_products_id_seq'::regclass);


--
-- TOC entry 2963 (class 2604 OID 26526)
-- Name: id; Type: DEFAULT; Schema: public; Owner: sysadm
--

ALTER TABLE ONLY payment_services ALTER COLUMN id SET DEFAULT nextval('payment_services_id_seq'::regclass);


--
-- TOC entry 2960 (class 2604 OID 26469)
-- Name: id; Type: DEFAULT; Schema: public; Owner: sysadm
--

ALTER TABLE ONLY payment_transactions ALTER COLUMN id SET DEFAULT nextval('payment_transactions_id_seq'::regclass);


--
-- TOC entry 2965 (class 2604 OID 26559)
-- Name: id; Type: DEFAULT; Schema: public; Owner: sysadm
--

ALTER TABLE ONLY terminals ALTER COLUMN id SET DEFAULT nextval('terminals_id_seq'::regclass);


--
-- TOC entry 2988 (class 2606 OID 26495)
-- Name: cards_pkey; Type: CONSTRAINT; Schema: public; Owner: sysadm
--

ALTER TABLE ONLY cards
    ADD CONSTRAINT cards_pkey PRIMARY KEY (id);


--
-- TOC entry 2979 (class 2606 OID 26455)
-- Name: items_pkey; Type: CONSTRAINT; Schema: public; Owner: sysadm
--

ALTER TABLE ONLY items
    ADD CONSTRAINT items_pkey PRIMARY KEY (id);


--
-- TOC entry 3004 (class 2606 OID 26550)
-- Name: merchants_pkey; Type: CONSTRAINT; Schema: public; Owner: sysadm
--

ALTER TABLE ONLY merchants
    ADD CONSTRAINT merchants_pkey PRIMARY KEY (id);


--
-- TOC entry 2974 (class 2606 OID 26438)
-- Name: orders_pkey; Type: CONSTRAINT; Schema: public; Owner: sysadm
--

ALTER TABLE ONLY orders
    ADD CONSTRAINT orders_pkey PRIMARY KEY (id);


--
-- TOC entry 2994 (class 2606 OID 26512)
-- Name: payment_products_pkey; Type: CONSTRAINT; Schema: public; Owner: sysadm
--

ALTER TABLE ONLY payment_products
    ADD CONSTRAINT payment_products_pkey PRIMARY KEY (id);


--
-- TOC entry 2999 (class 2606 OID 26531)
-- Name: payment_services_pkey; Type: CONSTRAINT; Schema: public; Owner: sysadm
--

ALTER TABLE ONLY payment_services
    ADD CONSTRAINT payment_services_pkey PRIMARY KEY (id);


--
-- TOC entry 2986 (class 2606 OID 26474)
-- Name: payment_transactions_pkey; Type: CONSTRAINT; Schema: public; Owner: sysadm
--

ALTER TABLE ONLY payment_transactions
    ADD CONSTRAINT payment_transactions_pkey PRIMARY KEY (id);


--
-- TOC entry 3009 (class 2606 OID 26564)
-- Name: terminals_pkey; Type: CONSTRAINT; Schema: public; Owner: sysadm
--

ALTER TABLE ONLY terminals
    ADD CONSTRAINT terminals_pkey PRIMARY KEY (id);


--
-- TOC entry 2989 (class 1259 OID 26496)
-- Name: index_cards_on_payment_transaction_id; Type: INDEX; Schema: public; Owner: sysadm
--

CREATE INDEX index_cards_on_payment_transaction_id ON cards USING btree (payment_transaction_id);


--
-- TOC entry 2975 (class 1259 OID 26456)
-- Name: index_items_on_order_id; Type: INDEX; Schema: public; Owner: sysadm
--

CREATE INDEX index_items_on_order_id ON items USING btree (order_id);


--
-- TOC entry 2976 (class 1259 OID 26462)
-- Name: index_items_on_sku; Type: INDEX; Schema: public; Owner: sysadm
--

CREATE INDEX index_items_on_sku ON items USING btree (sku);


--
-- TOC entry 2977 (class 1259 OID 26463)
-- Name: index_items_on_uuid; Type: INDEX; Schema: public; Owner: sysadm
--

CREATE UNIQUE INDEX index_items_on_uuid ON items USING btree (uuid);


--
-- TOC entry 3000 (class 1259 OID 26552)
-- Name: index_merchants_on_deleted_at; Type: INDEX; Schema: public; Owner: sysadm
--

CREATE INDEX index_merchants_on_deleted_at ON merchants USING btree (deleted_at);


--
-- TOC entry 3001 (class 1259 OID 26551)
-- Name: index_merchants_on_number; Type: INDEX; Schema: public; Owner: sysadm
--

CREATE INDEX index_merchants_on_number ON merchants USING btree (number);


--
-- TOC entry 3002 (class 1259 OID 26553)
-- Name: index_merchants_on_uuid; Type: INDEX; Schema: public; Owner: sysadm
--

CREATE UNIQUE INDEX index_merchants_on_uuid ON merchants USING btree (uuid);


--
-- TOC entry 2967 (class 1259 OID 26444)
-- Name: index_orders_on_deleted_at; Type: INDEX; Schema: public; Owner: sysadm
--

CREATE INDEX index_orders_on_deleted_at ON orders USING btree (deleted_at);


--
-- TOC entry 2968 (class 1259 OID 26439)
-- Name: index_orders_on_external_id; Type: INDEX; Schema: public; Owner: sysadm
--

CREATE INDEX index_orders_on_external_id ON orders USING btree (external_id);


--
-- TOC entry 2969 (class 1259 OID 26440)
-- Name: index_orders_on_number; Type: INDEX; Schema: public; Owner: sysadm
--

CREATE INDEX index_orders_on_number ON orders USING btree (number);


--
-- TOC entry 2970 (class 1259 OID 26441)
-- Name: index_orders_on_reference; Type: INDEX; Schema: public; Owner: sysadm
--

CREATE INDEX index_orders_on_reference ON orders USING btree (reference);


--
-- TOC entry 2971 (class 1259 OID 26442)
-- Name: index_orders_on_status; Type: INDEX; Schema: public; Owner: sysadm
--

CREATE INDEX index_orders_on_status ON orders USING btree (status);


--
-- TOC entry 2972 (class 1259 OID 26443)
-- Name: index_orders_on_uuid; Type: INDEX; Schema: public; Owner: sysadm
--

CREATE UNIQUE INDEX index_orders_on_uuid ON orders USING btree (uuid);


--
-- TOC entry 2990 (class 1259 OID 26520)
-- Name: index_payment_products_on_name; Type: INDEX; Schema: public; Owner: sysadm
--

CREATE INDEX index_payment_products_on_name ON payment_products USING btree (name);


--
-- TOC entry 2991 (class 1259 OID 26519)
-- Name: index_payment_products_on_number; Type: INDEX; Schema: public; Owner: sysadm
--

CREATE INDEX index_payment_products_on_number ON payment_products USING btree (number);


--
-- TOC entry 2992 (class 1259 OID 26513)
-- Name: index_payment_products_on_payment_transaction_id; Type: INDEX; Schema: public; Owner: sysadm
--

CREATE INDEX index_payment_products_on_payment_transaction_id ON payment_products USING btree (payment_transaction_id);


--
-- TOC entry 2995 (class 1259 OID 26539)
-- Name: index_payment_services_on_name; Type: INDEX; Schema: public; Owner: sysadm
--

CREATE INDEX index_payment_services_on_name ON payment_services USING btree (name);


--
-- TOC entry 2996 (class 1259 OID 26538)
-- Name: index_payment_services_on_number; Type: INDEX; Schema: public; Owner: sysadm
--

CREATE INDEX index_payment_services_on_number ON payment_services USING btree (number);


--
-- TOC entry 2997 (class 1259 OID 26532)
-- Name: index_payment_services_on_payment_product_id; Type: INDEX; Schema: public; Owner: sysadm
--

CREATE INDEX index_payment_services_on_payment_product_id ON payment_services USING btree (payment_product_id);


--
-- TOC entry 2980 (class 1259 OID 26484)
-- Name: index_payment_transactions_on_authorization_code; Type: INDEX; Schema: public; Owner: sysadm
--

CREATE INDEX index_payment_transactions_on_authorization_code ON payment_transactions USING btree (authorization_code);


--
-- TOC entry 2981 (class 1259 OID 26483)
-- Name: index_payment_transactions_on_number; Type: INDEX; Schema: public; Owner: sysadm
--

CREATE INDEX index_payment_transactions_on_number ON payment_transactions USING btree (number);


--
-- TOC entry 2982 (class 1259 OID 26475)
-- Name: index_payment_transactions_on_order_id; Type: INDEX; Schema: public; Owner: sysadm
--

CREATE INDEX index_payment_transactions_on_order_id ON payment_transactions USING btree (order_id);


--
-- TOC entry 2983 (class 1259 OID 26482)
-- Name: index_payment_transactions_on_terminal_number; Type: INDEX; Schema: public; Owner: sysadm
--

CREATE INDEX index_payment_transactions_on_terminal_number ON payment_transactions USING btree (terminal_number);


--
-- TOC entry 2984 (class 1259 OID 26481)
-- Name: index_payment_transactions_on_uuid; Type: INDEX; Schema: public; Owner: sysadm
--

CREATE UNIQUE INDEX index_payment_transactions_on_uuid ON payment_transactions USING btree (uuid);


--
-- TOC entry 3005 (class 1259 OID 26565)
-- Name: index_terminals_on_merchant_id; Type: INDEX; Schema: public; Owner: sysadm
--

CREATE INDEX index_terminals_on_merchant_id ON terminals USING btree (merchant_id);


--
-- TOC entry 3006 (class 1259 OID 26571)
-- Name: index_terminals_on_number; Type: INDEX; Schema: public; Owner: sysadm
--

CREATE INDEX index_terminals_on_number ON terminals USING btree (number);


--
-- TOC entry 3007 (class 1259 OID 26572)
-- Name: index_terminals_on_uuid; Type: INDEX; Schema: public; Owner: sysadm
--

CREATE UNIQUE INDEX index_terminals_on_uuid ON terminals USING btree (uuid);


--
-- TOC entry 2966 (class 1259 OID 26425)
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: sysadm
--

CREATE UNIQUE INDEX unique_schema_migrations ON schema_migrations USING btree (version);


--
-- TOC entry 3011 (class 2606 OID 26476)
-- Name: fk_rails_1ccedddf37; Type: FK CONSTRAINT; Schema: public; Owner: sysadm
--

ALTER TABLE ONLY payment_transactions
    ADD CONSTRAINT fk_rails_1ccedddf37 FOREIGN KEY (order_id) REFERENCES orders(id);


--
-- TOC entry 3013 (class 2606 OID 26514)
-- Name: fk_rails_2147054422; Type: FK CONSTRAINT; Schema: public; Owner: sysadm
--

ALTER TABLE ONLY payment_products
    ADD CONSTRAINT fk_rails_2147054422 FOREIGN KEY (payment_transaction_id) REFERENCES payment_transactions(id);


--
-- TOC entry 3015 (class 2606 OID 26566)
-- Name: fk_rails_279ea92902; Type: FK CONSTRAINT; Schema: public; Owner: sysadm
--

ALTER TABLE ONLY terminals
    ADD CONSTRAINT fk_rails_279ea92902 FOREIGN KEY (merchant_id) REFERENCES merchants(id);


--
-- TOC entry 3012 (class 2606 OID 26497)
-- Name: fk_rails_38a89d5f87; Type: FK CONSTRAINT; Schema: public; Owner: sysadm
--

ALTER TABLE ONLY cards
    ADD CONSTRAINT fk_rails_38a89d5f87 FOREIGN KEY (payment_transaction_id) REFERENCES payment_transactions(id);


--
-- TOC entry 3014 (class 2606 OID 26533)
-- Name: fk_rails_43e6fbeb5f; Type: FK CONSTRAINT; Schema: public; Owner: sysadm
--

ALTER TABLE ONLY payment_services
    ADD CONSTRAINT fk_rails_43e6fbeb5f FOREIGN KEY (payment_product_id) REFERENCES payment_products(id);


--
-- TOC entry 3010 (class 2606 OID 26457)
-- Name: fk_rails_53153f3b4b; Type: FK CONSTRAINT; Schema: public; Owner: sysadm
--

ALTER TABLE ONLY items
    ADD CONSTRAINT fk_rails_53153f3b4b FOREIGN KEY (order_id) REFERENCES orders(id);


--
-- TOC entry 3136 (class 0 OID 0)
-- Dependencies: 7
-- Name: public; Type: ACL; Schema: -; Owner: sysadm
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM sysadm;
GRANT ALL ON SCHEMA public TO sysadm;
GRANT ALL ON SCHEMA public TO PUBLIC;
GRANT ALL ON SCHEMA public TO rds_superuser;


-- Completed on 2016-10-18 15:10:21

--
-- PostgreSQL database dump complete
--

