output "db_username" {
    value = "${var.db_username}"
}
output "db_password" {
    value = "${var.db_password}"
}
output "db_name" {
    value = "${aws_db_instance.rds_order_instance.identifier}"
}
output "db_endpoint" {
    value = "${aws_db_instance.rds_order_instance.endpoint}"
}
