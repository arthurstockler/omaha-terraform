variable "key_name" {
		description = "nome da chave de acesso a instancia"
}

variable "sg_nat" {
		description = "para permitir acesso a saida para internet"
}

variable "sg_admin" {
		description = "para permitir acesso SSH as maquinas"
}

variable "sg_pg_admin" {
		description = "para permitir acesso ao pgadmin / psql"
}

variable "vpcid" {
		description = "Idetificador da VPC"
}

variable "subnet_rdsa_id" {
		description = "Subnet A para RDS (obrigatorio duas)"
}

variable "subnet_rdsd_id" {
		description = "Subnet D para RDS (obrigatorio duas)"
}

variable "weba_cidr_block" {
		description = "Bloco de CIDR para rota interna da subnet web para BD"
}

variable "rdsa_cidr_block" {
		description = "Bloco de CIDR para rota interna da subnet plsql"
}

variable "rdsd_cidr_block" {
		description = "Bloco de CIDR para rota interna da subnet plsql"
}

variable "aws_region" {
		description = "Região para dev N. Virginia"
}

variable "ec2_cli_access_key" {
		description = "Chave usada pelas instancias EC2 terem acesso ao aws cli"
}

variable "ec2_cli_secret_key" {
		description = "Chave usada pelas instancias EC2 terem acesso ao aws cli"
}

variable "db_username" {
		description = "Nome do usuario de banco de dados"
		default = "myuser"
}

variable "db_password" {
		description = "Senha root do banco de dados"
		default = "changepass"
}

variable "db_ddl" {
		description = "Nome do arquivo no S3 DDL do order manager"
		default = "order_ddl.sql"
}
