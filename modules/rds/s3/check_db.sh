#!/bin/sh -x
TF_DBNAME=$1
TF_DDLFILE=$2
TF_DBENDPOINT=$3
TF_DBUSER=$4

# espera banco esta disponivel
RETRY_COUNT=900;
until test $RETRY_COUNT -eq 0; do

  echo "verifica se existe o RDS" >> /home/ec2-user/trace.log

  aws rds describe-db-instances --db-instance-identifier=$TF_DBNAME --query 'DBInstances[0].[DBInstanceStatus]' --output json > rdscheck

  if [ ! -z $(grep "available" "rdscheck") ]; then
    echo "DBNAME available" >> /home/ec2-user/trace.log
    break;
  else
    echo "error.." >> /home/ec2-user/trace.log
    sleep 10;
    RETRY_COUNT=$(($RETRY_COUNT-1));
  fi
done;

echo ">> COPIANDO DDL S3" >> /home/ec2-user/trace.log
RETRY_COUNT=900;
until test $RETRY_COUNT -eq 0; do

  S3_DDL=$(aws s3 ls s3://cielo-omaha-staging/$TF_DDLFILE)
  if test -n "$S3_DDL"; then
    echo "S3_DDL $TF_DDLFILE" >> /home/ec2-user/trace.log
    break;
  else
    echo "esperando s3" >> /home/ec2-user/trace.log
    sleep 5;
    RETRY_COUNT=$(($RETRY_COUNT-1));
  fi;
done;
aws s3 cp s3://cielo-omaha-staging/$TF_DDLFILE /home/ec2-user/$TF_DDLFILE

echo ">> VERIFICANDO SE EXISTE TABELAS CRIADAS" >> /home/ec2-user/trace.log

TEST_COUT_TABLE=$(psql -U $TF_DBUSER -h $TF_DBENDPOINT -p 5432 -d postgres -P t -P format=unaligned -c \""SELECT count(1) FROM information_schema.tables WHERE table_schema='public'\"")

if [[ $TEST_COUT_TABLE -eq 0 ]]; then
  echo ">> IMPORTANDO DDL" >> /home/ec2-user/trace.log
  psql -U $TF_DBUSER -h $TF_DBENDPOINT -p 5432 -d postgres < /home/ec2-user/$TF_DDLFILE
else
  echo ">> BANCO JA ESTA CRIADO COM AS TABELAS" >> /home/ec2-user/trace.log
fi;

echo ">> FINALIZANDO SHELL << " >> /home/ec2-user/trace.log
