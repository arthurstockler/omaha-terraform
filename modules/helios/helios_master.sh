#!/bin/sh -x

 # Wait for internet connectivity
RETRY_COUNT=900;
until test $$RETRY_COUNT -eq 0; do
  if ping -c1 8.8.8.8 > /dev/null; then
    break;
  elif ping -c1 8.8.4.4 > /dev/null; then
    break;
  else
    sleep 1;
    RETRY_COUNT=$$(($RETRY_COUNT-1));
  fi;
done;


sudo su

### Install Docker
curl -sSL https://get.docker.com/ | sh
groupadd docker
usermod -aG docker ec2-user
service docker start
yum -y update

####COLOCANDO DOCKER ZOOKEEPER NA INICIALIZACAO
cat << EOF > /etc/systemd/system/docker-zookeeper_server.service

[Unit]
Description=Jmeter Container
Requires=docker.service
After=docker.service

[Service]
Restart=always
ExecStart=/usr/bin/docker start -a zookeeper
ExecStop=/usr/bin/docker stop -t 2 zookeeper

[Install]
WantedBy = multi-user.target

EOF

####COLOCANDO DOCKER HELIOS NA INICIALIZACAO
cat << EOF > /etc/systemd/system/docker-helios_server.service

[Unit]
Description=Jmeter Container
Requires=docker.service
After=docker.service

[Service]
Restart=always
ExecStart=/usr/bin/docker start -a helios-master
ExecStop=/usr/bin/docker stop -t 2 helios-master

[Install]
WantedBy = multi-user.target

EOF

systemctl enable docker-zookeeper_server.service
systemctl enable docker-helios_server.service

systemctl daemon-reload
systemctl start docker-zookeeper_server.service
systemctl start docker-helios_server.service


HOSTNAME=$(hostname -I | awk '{print $1}')

docker run -d --restart=always -p 2181:2181 --name zookeeper jplock/zookeeper:3.4.8
sleep 5
docker run -d --restart=always -p 5801:5801 --name helios-master spotify/helios-master --zk $HOSTNAME:2181
