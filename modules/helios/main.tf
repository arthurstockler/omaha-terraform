variable "key_name" {}

variable "sg_nat" {
	description = "security group para rede interna"
}

variable "sg_admin" {
	description = "para permitir acesso SSH as maquinas"
}

variable "subnet_id" {
	description = "Id da subnet privada de web"
}

variable "sg_helios" {
	description = "helios"
}

variable "subnet_cidr" {
	description = "Bloco CIDR da subnet do helios"
}
