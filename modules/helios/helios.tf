resource "aws_instance" "helios-master" {
    ami                    = "ami-c481fad3"
    instance_type          = "t2.nano"
    subnet_id              = "${var.subnet_id}"
    key_name               = "${var.key_name}"
    source_dest_check      = false
    vpc_security_group_ids = ["${var.sg_nat}", "${var.sg_admin}", "${var.sg_helios}"]
    private_ip             = "${cidrhost(var.subnet_cidr,4)}"
    user_data              = "${data.template_file.helios-master.rendered}"
    tags {
        "Name"        = "helios"
        "client"      = "cielo-lio"
        "role"        = "lab"
        "project"     = "backendlio"
    }
}

data "template_file" "helios-master" {
    template = "${file("${path.module}/helios_master.sh")}"
}
