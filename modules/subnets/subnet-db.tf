resource "aws_subnet" "rds_a" {
    vpc_id                  = "${var.vpcid}"
    availability_zone       = "${element(var.availability_zones, 0)}"
    cidr_block              = "${cidrsubnet(var.vpc_cidr_block,8,2)}"
    map_public_ip_on_launch = false
    tags {
        Name = "rds-a"
    }
}

resource "aws_subnet" "rds_d" {
    vpc_id                  = "${var.vpcid}"
    availability_zone       = "${element(var.availability_zones, 1)}"
    cidr_block              = "${cidrsubnet(var.vpc_cidr_block,8,18)}"
    map_public_ip_on_launch = false
    tags {
        Name = "rds-d"
    }
}

resource "aws_route_table_association" "rds_a" {
    subnet_id      = "${aws_subnet.rds_a.id}"
    route_table_id = "${aws_route_table.main_route_table_rds.id}"
}


resource "aws_route_table_association" "rds_d" {
    subnet_id      = "${aws_subnet.rds_d.id}"
    route_table_id = "${aws_route_table.main_route_table_rds.id}"
}

resource "aws_route_table" "main_route_table_rds" {
    vpc_id         = "${var.vpcid}"

    /*Rota para NAT A*/
    route {
        cidr_block = "0.0.0.0/0"
        instance_id = "${aws_instance.nat_a.id}"
    }

    /*Rota para BFA*/
    route {
        cidr_block  = "${var.vpn_cidr}"
        instance_id = "${var.vpnid}"/*Instancia de VPN*/
    }

    tags {
        Name = "route-rds"
    }
}
