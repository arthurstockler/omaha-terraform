resource "aws_subnet" "web_a" {
    vpc_id                  = "${var.vpcid}"
    availability_zone       = "${element(var.availability_zones, 0)}"
    cidr_block              = "${cidrsubnet(var.vpc_cidr_block,8,1)}"
    depends_on              = ["aws_internet_gateway.gateway-nat"]
    map_public_ip_on_launch = false
    tags {
        Name = "web-a"
    }
}

resource "aws_route_table_association" "web_a" {
    subnet_id      = "${aws_subnet.web_a.id}"
    route_table_id = "${aws_route_table.main_route_table_web.id}"
}

resource "aws_route_table" "main_route_table_web" {
	vpc_id         = "${var.vpcid}"

  	/*Rota para NAT A*/
  	route {
          cidr_block = "0.0.0.0/0"
          instance_id = "${aws_instance.nat_a.id}"
  	}

    /*Rota para BFA*/
    route {
        cidr_block  = "${var.vpn_cidr}"
        instance_id = "${var.vpnid}"/*Instancia de VPN*/
    }

    tags {
        Name = "route-web"
    }
}
