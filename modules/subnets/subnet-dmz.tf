resource "aws_subnet" "dmz_a" {
    vpc_id                  = "${var.vpcid}"
    availability_zone       = "${element(var.availability_zones, 0)}"
    cidr_block              = "${cidrsubnet(var.vpc_cidr_block,8,0)}"
    depends_on              = ["aws_internet_gateway.gateway-nat"]
    map_public_ip_on_launch = true
    tags {
        Name = "dmz-a"
    }
}

resource "aws_route_table_association" "dmz_a" {
    subnet_id      = "${aws_subnet.dmz_a.id}"
    route_table_id = "${aws_route_table.main_route_table_dmz.id}"
}


resource "aws_route_table" "main_route_table_dmz" {
    vpc_id         = "${var.vpcid}"

  	/*Rota para internet*/
  	route {
          cidr_block = "0.0.0.0/0"
          gateway_id = "${aws_internet_gateway.gateway-nat.id}"
  	}

    /*Rota para BFA*/
    route {
        cidr_block  = "${var.vpn_cidr}"
        instance_id = "${var.vpnid}"/*Instancia de VPN*/
    }
    tags {
        Name = "route-dmz"
    }
}

/* Elastic IP */
resource "aws_eip" "nat" {
    vpc      = true
}

/* Gateway internet */
resource "aws_internet_gateway" "gateway-nat" {
	vpc_id   = "${var.vpcid}"
	tags {
		Name = "nat-gateway"
		Tier = "Infrastructure"
		Zone = "-"
	}
}

/* Instancia de NAT */
resource "aws_instance" "nat_a" {
    ami                         = "${data.aws_ami.amazon_linux_ami_nat.image_id}"
    availability_zone           = "${element(var.availability_zones, 0)}"
    instance_type               = "t2.micro"
    key_name                    = "${var.key_name}"
    vpc_security_group_ids      = ["${var.sg_nat}"]
    subnet_id                   = "${aws_subnet.dmz_a.id}"
    associate_public_ip_address = true
    source_dest_check           = false
    tags {
        Name = "nat_a"
    }
}

resource "aws_eip_association" "eip_nat_a" {
  instance_id   = "${aws_instance.nat_a.id}"
  allocation_id = "${aws_eip.nat.id}"
}

data "aws_ami" "amazon_linux_ami_nat" {
  most_recent = true
  filter {
    name = "owner-alias"
    values = [
      "amazon"]
  }
  filter {
    name = "name"
    values = [
      "amzn-ami-vpc-nat-hvm-*-ebs"]
  }
}
