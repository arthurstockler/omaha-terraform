resource "aws_subnet" "master_control" {
    vpc_id                  = "${var.vpcid}"
    availability_zone       = "${element(var.availability_zones, 0)}"
    cidr_block              = "${cidrsubnet(var.vpc_cidr_block,8,3)}"
    map_public_ip_on_launch = false
    tags {
        Name = "master-control"
    }
    depends_on = ["aws_internet_gateway.gateway-nat"]
}

resource "aws_route_table_association" "master_control" {
    subnet_id      = "${aws_subnet.master_control.id}"
    route_table_id = "${aws_route_table.main_route_table_control.id}"
}

resource "aws_route_table" "main_route_table_control" {
    vpc_id         = "${var.vpcid}"

    /*Rota para NAT A*/
    route {
        cidr_block = "0.0.0.0/0"
        instance_id = "${aws_instance.nat_a.id}"
    }

    /*Rota para BFA*/
    route {
        cidr_block  = "${var.vpn_cidr}"
        instance_id = "${var.vpnid}"/*Instancia de VPN*/
    }

    tags {
        Name = "route-control"
    }
}
