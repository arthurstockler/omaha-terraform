variable "vpcid" {}
variable "vpc_cidr_block" {} 
variable "vpnid" {}
variable "vpn_cidr" {}
variable "sg_nat" {}
variable "key_name" {}
variable "availability_zones" {default = []}