output "subnet_dmz_a_id" {
	value = "${aws_subnet.dmz_a.id}"
}

output "nat_a_id" {
	value = "${aws_instance.nat_a.id}"
}

output "gateway_nat_id" {
	value = "${aws_internet_gateway.gateway-nat.id}"
}


output "subnet_web_a_id" {
	value = "${aws_subnet.web_a.id}"
}
output "weba_cidr_block" {
    value = "${aws_subnet.web_a.cidr_block}"
}

output "subnet_rdsa_id" {
    value = "${aws_subnet.rds_a.id}"
}
output "subnet_rdsd_id" {
    value = "${aws_subnet.rds_d.id}"
}
output "rdsa_cidr_block" {
    value = "${aws_subnet.rds_a.cidr_block}"
}
output "rdsd_cidr_block" {
    value = "${aws_subnet.rds_d.cidr_block}"
}


output "subnet_master_control_id" {
    value = "${aws_subnet.master_control.id}"
}
output "master_cidr_block" {
    value = "${aws_subnet.master_control.cidr_block}"
}


output "jmt_slaves_id" {
	value = "${aws_subnet.jmt_slaves.id}"
}
