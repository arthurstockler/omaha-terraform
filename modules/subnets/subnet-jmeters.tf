resource "aws_subnet" "jmt_slaves" {
    vpc_id                  = "${var.vpcid}"
    availability_zone       = "${element(var.availability_zones, 0)}"
    cidr_block              = "${cidrsubnet(var.vpc_cidr_block,8,4)}"
    map_public_ip_on_launch = false
    tags {
        Name = "jmeter-slaves"
    }
}

resource "aws_route_table_association" "jmt_slaves" {
    subnet_id      = "${aws_subnet.jmt_slaves.id}"
    route_table_id = "${aws_route_table.main_route_table_jmeter_slaves.id}"
}

resource "aws_route_table" "main_route_table_jmeter_slaves" {
    vpc_id         = "${var.vpcid}"

  	/*Rota para NAT A*/
  	route {
          cidr_block = "0.0.0.0/0"
          instance_id = "${aws_instance.nat_a.id}"
  	}

    /*Rota para BFA*/
    route {
        cidr_block  = "${var.vpn_cidr}"
        instance_id = "${var.vpnid}"/*Instancia de VPN*/
    }

    tags {
        Name = "route-jmeters-slaves"
    }
}
