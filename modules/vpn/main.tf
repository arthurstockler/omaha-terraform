variable "vpn_ip" {}
variable "vpn_psk" {}
variable "vpn_cidr" {}
variable "vpc_cidr_block" {}
variable "m4u_team_id" {}
variable "subnet_dmza" {}
variable "key_name" {}
variable "vpc_security_group_ids" {}