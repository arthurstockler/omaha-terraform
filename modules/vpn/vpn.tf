data "template_file" "ipsec_conf" {
        template = "${file("${path.module}/ipsec-conf.sh")}"
        vars {
                vpn_ip     = "${var.vpn_ip}"
                vpn_psk    = "${var.vpn_psk}"
                vpn_cidr   = "${var.vpn_cidr}"
                vpn_subnet = "${var.vpc_cidr_block}"
                cell_id    = "${var.m4u_team_id}"
        }
}

resource "aws_instance" "vpn-instance" {
    ami                         = "${data.aws_ami.amazon_linux_ami.image_id}"
    instance_type               = "t2.micro"
    source_dest_check           = "false"
    associate_public_ip_address = "true"
    subnet_id                   = "${var.subnet_dmza}"
    key_name                    = "${var.key_name}"
    user_data                   = "${data.template_file.ipsec_conf.rendered}"
    #private_ip                  = "${cidrhost(cidrsubnet(var.vpc_cidr_block,5,0),5)}"
    vpc_security_group_ids      = ["${split(",", var.vpc_security_group_ids)}"]

    tags {
    	   Name                   = "vpn"
    }
}

data "aws_ami" "amazon_linux_ami" {
  most_recent = true
  filter {
    name = "owner-alias"
    values = [
      "amazon"]
  }
  filter {
    name = "name"
    values = [
      "amzn-ami-hvm-*-ebs"]
  }
}
