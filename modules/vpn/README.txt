sudo ipsec verify
(checks the status of the services required for OpenSWAN to run properly)

sudo service ipsec status
(checks the status of the OpenSWAN service and the VPN tunnels)
