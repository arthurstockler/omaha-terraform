resource "aws_vpc" "main" {
  cidr_block = "${var.vpc_cidr_block}"
  tags {
    Name = "vpc-test-stress"
    Tier = "Infrastructure"
    Zone = "-"
  }
}